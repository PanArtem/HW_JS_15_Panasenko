$(".btn-slide").click(() => {
    $('.top-head').slideToggle("slow");
    $(this).toggleClass("active");
    return false;
});

$(window).on('scroll', () => {
    if (window.pageYOffset >= 660) {
        $('.btn-top ').css('display', 'inline-block');
    } else {
        $('.btn-top ').css('display', 'none');
    }
    return false;
});

$('a[data-target^="anchor"]').bind('click.smoothscroll', function(){
    let target = $(this).attr('href');
    let bl_top = $(target).offset().top;
   $('body, html').animate({scrollTop:bl_top}, 1000);
    return false;
});

